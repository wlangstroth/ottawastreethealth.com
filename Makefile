deploy:
	./build
	rsync -av site.static/ sites:/var/www/ottawastreethealth.com/
rebuild:
	clean && ./build
clean:
	rm -rf site.static/*
